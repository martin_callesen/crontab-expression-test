package dk.martincallesen.crontab;

import org.junit.Before;
import org.junit.Test;

import static dk.martincallesen.crontab.CrontabExpressionValidator.NO_RUN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CrontabExpressionValidatorTest {
    private CrontabExpressionValidator validator;

    @Before
    public void createValidator(){
        validator = new CrontabExpressionValidator();
    }

    @Test
    public void validExpression() throws Exception {
        String expression = "0 0/5 * * * ?";
        boolean validExpression = validator.isValidExpression(expression);
        assertTrue("Crontab expression", validExpression);
    }

    @Test
    public void runInYear2099(){
        String expression = "0 0 0 * * ? 2099";
        String nextRunIsAt = validator.nextRunIsAt(expression);
        assertEquals("Next run", "Thu Jan 01 00:00:00 CET 2099", nextRunIsAt);
    }

    @Test
    public void neverToRun(){
        String expression = "0 0 0 * * ? 1970";
        String nextRunIsAt = validator.nextRunIsAt(expression);
        assertEquals("Next run", NO_RUN, nextRunIsAt);
    }
}