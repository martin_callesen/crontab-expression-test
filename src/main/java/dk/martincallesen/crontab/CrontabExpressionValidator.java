package dk.martincallesen.crontab;

import org.quartz.CronExpression;

import java.text.ParseException;
import java.util.Date;

public class CrontabExpressionValidator {
    public static final String NO_RUN = "";
    private Date currentDate;

    public CrontabExpressionValidator() {
        super();
    }

    public CrontabExpressionValidator(Date date) {
        this.currentDate = date;
    }

    public boolean isValidExpression(String expression){
        return CronExpression.isValidExpression(expression);
    }

    public String nextRunIsAt(String expression){
        try {
            if(isValidExpression(expression)) {
                CronExpression cronExpression = new CronExpression(expression);
                Date timeAfter = cronExpression.getTimeAfter(currentDate());

                return timeAfter != null ? timeAfter.toString() : NO_RUN;
            }
        } catch (ParseException e) {
            System.out.print(e);
        }

        return NO_RUN;
    }

    private Date currentDate() {
        Date returnedDate = new Date();

        if(currentDate != null){
            returnedDate = currentDate;
        }

        return returnedDate;
    }
}
